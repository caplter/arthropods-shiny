#' @title helper: add a new or update existing trap specimen record
#'
#' @description Functions to add a new or update existing record of
#' arthropods.trap_specimens

insert_new_or_update_specimen <- function(
  query_type,
  trap_specimen_id,
  tse_id,
  person_name,
  taxon,
  count,
  flag_immature,
  flag_winged,
  size
  ) {

  tse_id        <- as.integer(tse_id)
  person_id     <- observers[observers$observer_name == person_name, ][["id"]]
  person_id     <- as.integer(person_id)
  taxon_id      <- taxa[taxa$display_name == taxon, ][["id"]]
  taxon_id      <- as.integer(taxon_id)
  count         <- as.integer(count)
  flag_immature <- as.logical(flag_immature)
  flag_winged   <- as.logical(flag_winged)
  size_id       <- sizes[sizes$size_class == size, ][["id"]]
  size_id       <- as.integer(size_id)

  if (query_type == "insert") {

    base_query <- glue::glue_sql("
      INSERT INTO arthropods.trap_specimens
      (
        trap_sampling_event_id,
        person_id,
        arthropod_taxon_id,
        count,
        immature,
        winged,
        size_class_id
      )
      VALUES(
        { tse_id },
        { person_id },
        { taxon_id },
        { count },
        { flag_immature },
        { flag_winged },
        { size_id }
      )
      ;
      ",
      .con = DBI::ANSI()
    )

  } else if (query_type == "update") {

    base_query <- glue::glue_sql("
      UPDATE arthropods.trap_specimens
      SET
        person_id = { person_id },
        arthropod_taxon_id = { taxon_id },
        count = { count },
        immature = { flag_immature },
        winged = { flag_winged },
        size_class_id = { size_id }
      WHERE
        id = { trap_specimen_id }
      ;
      ",
      .con = DBI::ANSI()
    )

  } else {

    base_query <- NULL

  }

  # print(base_query)
  run_interpolated_execution(base_query)

}
