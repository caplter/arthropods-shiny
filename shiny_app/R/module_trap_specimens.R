#' @title module: trap specimens (main)
#'
#' @description The trap_specimens module facilitates viewing, adding new,
#' editing, and deleting arthropods.trap_specimens records.
#'
#' @note As with the trap sampling events, the modal module(s) for adding new
#' and editing existing specimens conflicted as initially implemented. As such,
#' as withing the trap sampling events module, these modal modules had to be
#' wrapped in an \code{observeEvent} and the inputs had to be removed. See
#' details outlined in the trap sampling events module
#' [documentation](https://gitlab.com/caplter/arthropods-shiny/-/blob/main/shiny_app/R/module_trap_sampling_events.R).

# trap sampling events UI ------------------------------------------------------

trap_specimensUI <- function(id) {

  ns <- shiny::NS(id)

  shiny::tagList(

    shiny::fluidRow(
      id    = "row_reference_trap_sampling_event_details",

      shiny::column(
        id     = "column_reference_trap_sampling_event_details",
        width  = 12,
        offset = 0,
        style = "padding-left: 30px; padding-right: 30px;",
        shiny::verbatimTextOutput(ns("trap_sampling_event_details_view"))
      ) # close column

      ), # close row

    shiny::fluidRow(
      id = "row_trap_specimens_events",

      shiny::column(
        id     = "column_trap_specimens_buttons",
        width  = 2,
        offset = 0,
        style = "padding-left: 30px; padding-right: 10px;",
        shiny::br(),
        shiny::br(),
        shiny::actionButton(
          inputId = ns("add_trap_specimen"),
          label   = "add trap specimen",
          class   = "btn-success",
          style   = "color: #fff; margin-bottom: 2px;",
          icon    = shiny::icon("plus"),
          width   = "100%"
          ),
        shiny::br(),
        shiny::br(),
        shiny::selectInput(
          inputId   = ns("default_observer"),
          label     = "default observer",
          choices   = observers[["observer_name"]],
          selected  = NULL,
          multiple  = FALSE,
          selectize = FALSE
        )
        ),

      shiny::column(
        id     = "column_trap_specimens",
        width  = 10,
        offset = 0,
        style = "padding-right: 30px;",
        DT::DTOutput(ns("trap_specimens_view"))
      ) # close column

      ), # close row

    # js file and function within file respectively
    tags$script(src = "trap_specimens_module.js"),
    tags$script(paste0("trap_specimens_module_js('", ns(''), "')"))

  ) # close tagList

} # close UI


# trap sampling events ---------------------------------------------------------

trap_specimens <- function(id, tse_to_populate) {

  shiny::moduleServer(id, function(input, output, session) {

    # added to facilitate renderUIs
    ns <- session$ns

    # message("from ts ", session$ns(id))

    # query trap sampling event details for reference
    trap_sampling_event_details_reactive <- shiny::reactive({

      trap_sampling_event_details <- query_trap_sampling_event(
        tse_id = tse_to_populate()$id
        ) |>
      dplyr::select(-flags)

      return(trap_sampling_event_details)

    })

    output$trap_sampling_event_details_view <- shiny::renderText({

      paste0(
        "trap sampling event: ", tse_to_populate()$id, " | ",
        "site: ", trap_sampling_event_details_reactive()[["site_code"]], " | ",
        "date: ", trap_sampling_event_details_reactive()[["sample_date"]], " | ",
        "trap: ", trap_sampling_event_details_reactive()[["trap_name"]]
      )

    })


    # query trap specimens
    trap_specimens_reactive <- shiny::reactive({

      listener_watch("update_specimen")

      trap_specimens_queried <- query_trap_specimens(
        tse_id = tse_to_populate()$id
      )

      if (nrow(trap_specimens_queried) == 0) {

        trap_specimens_queried <- NULL

      } else {

        actions <- purrr::map_chr(trap_specimens_queried$id, function(id_) {
          paste0(
            '<div class="btn-group" style="width: 120px;" role="group" aria-label="Basic example">
              <button class="btn btn-primary btn-sm edit_btn" data-toggle="tooltip" data-placement="top" title="Edit" id = ', id_, ' style="margin: 0"><i class="fa fa-pencil-square-o"></i></button>
              <button class="btn btn-danger btn-sm delete_btn" data-toggle="tooltip" data-placement="top" title="Delete" id = ', id_, ' style="margin-left: 5px;"><i class="fa fa-trash-o"></i></button>
            </div>'
          )
      }
        )

        trap_specimens_queried <- cbind(
          tibble::tibble("actions" = actions),
          trap_specimens_queried
        )

      }

      return(trap_specimens_queried)

    })


    # render editable table of trap sampling events data
    output$trap_specimens_view <- DT::renderDT({

      trap_specimens_reactive()

    },
    class      = "cell-border stripe",
    plugins    = c("ellipsis"),
    escape     = FALSE,
    selection  = "none",
    rownames   = FALSE,
    options    = list(
      scrollX       = TRUE,
      bFilter       = 0,
      bLengthChange = FALSE,
      bPaginate     = FALSE,
      bSort         = TRUE,
      autoWidth     = FALSE,
      columnDefs    = list(
        list(
          targets = c(1, 2, 3, 5, 10),
          visible = FALSE
          ),
        list(
          targets   = c(0),
          width     = "80px"
          ),
        list(
          targets   = c(0),
          className = "dt-center"
          ),
        list(
          targets   = c(0),
          orderable = FALSE
        )
      )
    )
    ) # close output$trap_specimens_view


    # module: edit specimen

    this_specimen_to_edit <- shiny::eventReactive(input$trap_specimen_id_to_edit, {

      this_trap_specimen <- query_trap_specimen(ts_id = input$trap_specimen_id_to_edit)

      return(this_trap_specimen)

    })


    edit_specimen_counter <- shiny::reactiveVal(value = 0)

    shiny::observeEvent(input$trap_specimen_id_to_edit, {

      id <- edit_specimen_counter()

      module_specimen_new(
        id               = paste0("edit_specimen", id),
        modal_title      = "edit trap specimen",
        tse_id           = tse_to_populate()$id,
        specimen_to_edit = this_specimen_to_edit,
        tse_observer     = input$default_observer
      )

      edit_specimen_counter(id + 1) # increment module counter

      if (edit_specimen_counter() > 1) {

        specimen_module_id <- paste0(
          "sampling_events-tse_inventory0-trap_specimens_inventory0-edit_specimen",
          id,
          "-edit_specimen",
          id
        )

        remove_shiny_inputs(specimen_module_id, input)

      }

    },
    once       = FALSE,
    ignoreInit = TRUE
    )



    # module: add new trap sampling event

    add_specimen_counter <- shiny::reactiveVal(value = 0)

    shiny::observeEvent(input$add_trap_specimen, {

      id <- add_specimen_counter()

      module_specimen_new(
        id               = paste0("add_trap_specimen", id),
        modal_title      = "add new trap specimen",
        tse_id           = tse_to_populate()$id,
        specimen_to_edit = function() NULL,
        tse_observer     = input$default_observer
      )

      add_specimen_counter(id + 1) # increment module counter

      if (add_specimen_counter() > 1) {

        specimen_module_id <- paste0(
          "sampling_events-tse_inventory0-trap_specimens_inventory0-add_trap_specimen",
          id,
          "-add_trap_specimen",
          id
        )

        remove_shiny_inputs(specimen_module_id, input)

      }

    },
    once       = FALSE,
    ignoreInit = TRUE
    )


    # delete trap specimen

    shiny::observeEvent(input$trap_specimen_id_to_delete, {

      tryCatch({

        delete_trap_specimen(ts_id = input$trap_specimen_id_to_delete)

        listener_trigger("update_specimen")

      }, warning = function(warn) {

        shiny::showNotification(
          ui          = paste("warning: ", warn),
          duration    = NULL,
          closeButton = TRUE,
          type        = "warning"
        )

      }, error = function(err) {

        shiny::showNotification(
          ui          = paste("error: ", err),
          duration    = NULL,
          closeButton = TRUE,
          type        = "error"
        )

      }) # close tryCatch

    }) # close delete trap specimen


    # debugging: module level -------------------------------------------------

    # print(head(taxa))
    # observe(print({ dplyr::glimpse(this_specimen_to_edit()) }))
    # observe(print({ tse_to_populate() }))
    # observe(print({ input$trap_specimen_id_to_edit }))


  }) # close module sever
} # close module function
