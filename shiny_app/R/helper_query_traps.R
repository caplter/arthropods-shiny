#' @title query arthropods trap details
#'
#' @description Makes available the list of trap details (id, site_id,
#' trap_name) for arthropod traps.
#'
#' @note Traps are filtered to actively surveyed sites.

query_traps <- function() {

  base_query <- glue::glue_sql("
    SELECT
      traps.id,
      traps.site_id,
      traps.trap_name,
      sites.site_code
    FROM arthropods.traps
    JOIN arthropods.sites ON (sites.id = traps.site_id)
    WHERE
      sites.end_date IS NULL
    ;
    ",
    .con = DBI::ANSI()
  )

  trap_details <- run_interpolated_query(base_query)

  return(trap_details)

}

traps <- query_traps()
