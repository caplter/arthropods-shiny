function trap_specimens_module_js(ns_prefix) {

  $("#" + ns_prefix + "trap_specimens_view").on("click", ".delete_btn", function() {
    Shiny.setInputValue(ns_prefix + "trap_specimen_id_to_delete", this.id, { priority: "event"});
    $(this).tooltip('hide');
  });

  $("#" + ns_prefix + "trap_specimens_view").on("click", ".edit_btn", function() {
    Shiny.setInputValue(ns_prefix + "trap_specimen_id_to_edit", this.id, { priority: "event"});
    $(this).tooltip('hide');
  });

  $("#" + ns_prefix + "trap_specimens_view").on("click", ".info_btn", function() {
    Shiny.setInputValue(ns_prefix + "trap_specimen_id_to_populate", this.id, { priority: "event"});
    $(this).tooltip('hide');
  });

}
