# arthropods-shiny

### R shiny application to facilitate CAP LTER arthropod monitoring data entry and quality control

This is the codebase for the Shiny application that facilitates data entry and quality control for the CAP LTER's long-term monitoring of arthropods in the greater Phoenix, Arizona, USA metropolitan area.

### overview

Replacing earlier Rails-based applications, this is the primary interface to the CAP LTER arthropods database. In keeping with overall [revisions](https://gitlab.com/caplter/arthropods-database) to the arthropod monitoring project, this application merges functionality provided by two applications that addressed the Core and McDowell Sonoran Preserve separately.

### application structure

The application features a nested structure that addresses sampling events > trap sampling events > trap specimens. Owing to the nested structure, extra care is required for modal modules that facilitate adding new and editing existing trap sampling events and trap specimens such that (1) modules are wrapped in `observeEvent` (rather than enabled by a function parameter) so that they are not instantiated at start, and (2) module inputs are explicitly removed (see the [documentation](https://gitlab.com/caplter/arthropods-shiny/-/blob/main/shiny_app/R/module_trap_sampling_events.R) for the trap sampling events module for additional detail).

![](assets/figures/structure.png)
